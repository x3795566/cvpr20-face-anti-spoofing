import os
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.optim
import numpy as np
from mimamo_net.config import parser
import torch.nn.functional as TF

from mimamo_net.dataloader import FaceFramesDataset
from mimamo_net.network import TwoStreamRNN
from tqdm import tqdm
from torch.utils.data import DataLoader
from torch import optim
from loss.cyclic_lr import CosineAnnealingLR_with_Restart
from train_CyclicLR import setupdirs
from utils import Logger
from utils import softmax_cross_entropy_criterion
from timeit import default_timer as timer
from metric import metric
from metric import calculate_accuracy
from metric import ACER
from utils import time_to_str
from process.data_helper import submission


# def softmax_cross_entropy_criterion(logit, truth, is_average=True):
#     loss = F.cross_entropy(logit, truth, reduce=is_average)
#     return loss

def infer_test(net, test_loader, device):
    test_num = 0
    probs = []
    # ids = []

    for i, data_batch in enumerate(tqdm(test_loader)):
        blocks, phase0, phase1, _ = data_batch
        B, N, F, n, C1, W1, H1 = blocks.size()
        _, _, _, C2, W2, H2 = phase0.size()
        _, _, _, C3, W3, H3 = phase1.size()

        blocks = blocks.view(B*N, F, n, C1, W1, H1)
        phase0 = phase0.view(B*N, F, C2, W2, H2)
        phase1 = phase1.view(B*N, F, C3, W3, H3)

        blocks = blocks.to(device)
        phase0 = phase0.to(device)
        phase1 = phase1.to(device)
        # print('B*N = ', B*N)
        with torch.no_grad():
            logit = net.forward([phase0, phase1], blocks)
            last_logit = logit[:, -1, :]

            last_logit = last_logit.view(B, N, 2)
            last_logit = torch.mean(last_logit, dim=1, keepdim=False)
            prob = TF.softmax(last_logit, 1)

        test_num += B*N
        probs.append(prob.data.cpu().numpy())
        # ids.append(id)

    # ids = np.concatenate(ids)
    probs = np.concatenate(probs)
    return probs[:, 1]


def do_test(config, dir):
    out_dir = './rnn_models_' + config.proto
    config.model_name = 'params_' + str(config.seqlen) + '_' + str(config.phase_num) + '_' + str(config.phase_size)
    out_dir = os.path.join(out_dir, config.model_name)
    initial_checkpoint = config.checkpoint_name
    criterion = softmax_cross_entropy_criterion
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    # net --------------------------------------------------------------------------------
    net = TwoStreamRNN(config.hidden_units, num_phase=config.phase_num)
    # net = torch.nn.DataParallel(net)
    net = net.to(device)

    if initial_checkpoint is not None:
        save_dir = os.path.join(out_dir + '/checkpoint', dir, initial_checkpoint)
        initial_checkpoint = os.path.join(out_dir + '/checkpoint', initial_checkpoint)
        print('\tinitial_checkpoint = %s\n' % initial_checkpoint)
        net.load_state_dict(torch.load(initial_checkpoint, map_location=lambda storage, loc: storage))
        if not os.path.exists(os.path.join(out_dir + '/checkpoint', dir)):
            os.makedirs(os.path.join(out_dir + '/checkpoint', dir))

    # dataset --------------------------------------------------------------------------------

    # get validation dataset
    val_dataset = FaceFramesDataset(mode='val', proto=config.proto, block_size=config.image_size, seqlen=config.seqlen, phase_num=config.phase_num,
                                    phase_size=config.phase_size)
    val_loader = DataLoader(val_dataset, shuffle=False, batch_size=1, drop_last=False, num_workers=8)

    # get test dataset
    test_dataset = FaceFramesDataset(mode='test', proto=config.proto, block_size=config.image_size, seqlen=config.seqlen, phase_num=config.phase_num,
                                     phase_size=config.phase_size)
    test_loader = DataLoader(test_dataset, shuffle=False, batch_size=1, drop_last=False, num_workers=8)

    net.eval()

    valid_loss, valid_acer, valid_acc, valid_correct, valid_probs = do_valid(net, val_loader, criterion, device)
    print('%0.6f  %0.6f  %0.3f  (%0.3f) \n' % (valid_loss, valid_acer, valid_acc, valid_correct))

    print('infer!!!!!!!!!')
    torch.cuda.empty_cache()
    test_probs = infer_test(net, test_loader, device)
    print('done')
    submission(valid_probs, test_probs, save_dir + '.txt', config.proto, video_mode=True)


def do_valid(net, valid_loader, criterion, device):
    valid_num = 0
    losses = []
    corrects = []
    probs = []
    labels = []

    for i, data_batch in enumerate(tqdm(valid_loader)):
        blocks, phase0, phase1, label = data_batch

        # b, n, c, w, h = input.size()
        # input = input.view(b * n, c, w, h)
        B, N, F, n, C1, W1, H1 = blocks.size()
        _, _, _, C2, W2, H2 = phase0.size()
        _, _, _, C3, W3, H3 = phase1.size()

        blocks = blocks.view(B*N, F, n, C1, W1, H1)
        phase0 = phase0.view(B*N, F, C2, W2, H2)
        phase1 = phase1.view(B*N, F, C3, W3, H3)
        # label = torch.cat([label] * (N*F), dim=1)
        label = label.view(-1)

        blocks = blocks.to(device)
        # centers = centers.to(device)
        phase0 = phase0.to(device)
        phase1 = phase1.to(device)
        label = label.to(device)

        with torch.no_grad():
            logit = net.forward([phase0, phase1], blocks)
            logit = logit.view(B, N*F, 2)
            logit = torch.mean(logit, dim=1, keepdim=False)

            label = label.view(logit.shape[0])
            loss = criterion(logit, label, False)
            correct, prob = metric(logit, label)

        valid_num += len(label)
        losses.append(loss.data.cpu().numpy())
        corrects.append(np.asarray(correct).reshape([1]))
        probs.append(prob.data.cpu().numpy())
        labels.append(label.data.cpu().numpy())

    # assert(valid_num == len(test_loader.sampler))
    # ----------------------------------------------

    correct = np.concatenate(corrects)
    loss = np.concatenate(losses)
    loss = loss.mean()
    correct = np.mean(correct)

    probs = np.concatenate(probs)
    labels = np.concatenate(labels)

    tpr, fpr, acc = calculate_accuracy(0.5, probs[:, 1], labels)
    acer, _, _, _, _ = ACER(0.5, probs[:, 1], labels)

    return loss, acer, acc, correct, probs[:, 1]


def do_train(config):
    out_dir = './rnn_models_' + config.proto
    config.model_name = 'params_' + str(config.seqlen) + '_' + str(config.phase_num) + '_' + str(config.phase_size)
    out_dir = os.path.join(out_dir, config.model_name)
    criterion = softmax_cross_entropy_criterion
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    # setup  -----------------------------------------------------------------------------
    setupdirs(out_dir)

    log = Logger()
    log.open(os.path.join(out_dir, config.model_name + '.txt'), mode='a')
    log.write('\tout_dir      = %s\n' % out_dir)
    log.write('\n')

    # dataset  ---------------------------------------------------------------------------
    log.write('** dataset setting **\n')

    # get train dataset
    train_dataset = FaceFramesDataset('train', config.proto, block_size=config.image_size, seqlen=config.seqlen,
                                      phase_num=config.phase_num, phase_size=config.phase_size)
    train_loader = DataLoader(train_dataset, shuffle=True, batch_size=config.batch_size, drop_last=True,
                              num_workers=4, pin_memory=False)

    # get validation dataset
    val_dataset = FaceFramesDataset('val', config.proto, block_size=config.image_size, seqlen=config.seqlen, phase_num=config.phase_num,
                                    phase_size=config.phase_size)
    val_loader = DataLoader(val_dataset, shuffle=False, batch_size=config.batch_size // 64, drop_last=False,
                            num_workers=4, pin_memory=False)

    log.write('batch_size = %d\n' % config.batch_size)
    log.write('train_dataset : \n%s\n' % train_dataset)
    log.write('valid_dataset : \n%s\n' % val_dataset)
    log.write('\n')

    # net  -------------------------------------------------------------------------------
    log.write('** net setting **\n')

    model = TwoStreamRNN(config.hidden_units, num_phase=config.phase_num)
    print(model)
    model.to(device)

    log.write('%s\n' % (type(model)))
    log.write('criterion=%s\n' % criterion)
    log.write('\n')

    # start training here! ---------------------------------------------------------------
    log.write('** start training here! **\n')
    log.write('                                  |------------ VALID -------------|-------- TRAIN/BATCH ----------|         \n')
    log.write('model_name   lr   iter  epoch     |     loss      acer      acc    |     loss              acc     |  time   \n')
    log.write('----------------------------------------------------------------------------------------------------\n')

    start_iter = 0
    iter = 0
    i = 0

    valid_loss = 0.0
    valid_acer = 0.0
    valid_acc = 0.0
    batch_loss = np.zeros(6, np.float32)

    start = timer()

    # optimizer --------------------------------------------------------------------------
    optimizer = optim.SGD(filter(lambda p: p.requires_grad, model.parameters()),
                          lr=0.1, momentum=0.9, weight_decay=0.0005)

    sgdr = CosineAnnealingLR_with_Restart(optimizer, T_max=config.cycle_inter, T_mult=1, model=model,
                                          out_dir='../input/',
                                          take_snapshot=False,
                                          eta_min=1e-3)

    global_min_acer = 1.0
    global_min_loss = 1.0
    for cycle_index in range(config.cycle_num):
        print('cycle index: ' + str(cycle_index))

        for epoch in range(config.cycle_inter):
            sgdr.step()
            lr = optimizer.param_groups[0]['lr']
            print('lr : {:.4f}'.format(lr))

            optimizer.zero_grad()
            for i, data_batch in enumerate(train_loader):
                iter = i + start_iter
                model.train()

                # img, _, phase0, phase1, label = data_batch
                blocks, phase0, phase1, label = data_batch
                blocks = blocks.to(device)
                # centers = centers.to(device)
                phase0 = phase0.to(device)
                phase1 = phase1.to(device)
                # expand label to sequence length
                batch_size, seqlen = phase0.shape[0:2]
                label = torch.cat([label] * seqlen, dim=1)
                label = label.view(batch_size * seqlen)
                label = label.to(device)

                logit = model.forward([phase0, phase1], blocks)
                # last_logit = logit[:, -1, :]
                logit = logit.view(batch_size*seqlen, -1)
                # label = label.view(logit.shape[0])
                loss = criterion(logit, label)
                precision, _ = metric(logit, label)

                loss.backward()
                optimizer.step()  # It have accumulated enough gradients
                optimizer.zero_grad()

                # print statistics  ------------
                batch_loss[:2] = np.array((loss.item(), precision.item(),))

            if epoch >= config.cycle_inter // 2:
                model.eval()
                valid_loss, valid_acer, valid_acc, _, _ = do_valid(model, val_loader, criterion, device)
                model.train()

                if valid_acer < global_min_acer and epoch > 0:
                    global_min_acer = valid_acer
                    ckpt_name = out_dir + '/checkpoint/global_min_acer_model.pth'
                    torch.save(model.state_dict(), ckpt_name)
                    log.write('save global min acer model: ' + str(global_min_acer) + '\n')
                elif valid_acer == 0 and epoch > 0 and valid_loss < global_min_loss:
                    global_min_loss = valid_loss
                    ckpt_name = out_dir + '/checkpoint/global_min_loss_model.pth'
                    torch.save(model.state_dict(), ckpt_name)
                    log.write('save global min loss model: ' + str(global_min_loss) + '\n')

            asterisk = ' '
            log.write(
                config.model_name + ' Cycle %d: %0.4f %5.1f %6.1f | %0.6f  %0.6f  %0.3f %s  | %0.6f  %0.6f |%s \n' % (
                    cycle_index, lr, iter, epoch,
                    valid_loss, valid_acer, valid_acc, asterisk,
                    batch_loss[0], batch_loss[1],
                    time_to_str((timer() - start), 'min')))



def main(config):
    if config.mode == 'train':
        do_train(config)
    elif config.mode == 'infer_test':
        do_test(config, dir='global_test_36_TTA')
    else:
        raise Exception('Unknown mode:', config.mode)


if __name__ == '__main__':
    config = parser.parse_args()
    print(config)
    main(config)
