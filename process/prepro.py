import numpy as np
from imutils.face_utils import FaceAligner as _FaceAligner
from imutils.face_utils import rect_to_bb
import dlib
import cv2


def cut_black_edge(image, thres=10):
    y_nonzero, x_nonzero, _ = np.nonzero(image > thres)
    return image[np.min(y_nonzero):np.max(y_nonzero), np.min(x_nonzero):np.max(x_nonzero)]


class FaceAligner:
    def __init__(self, size=224):
        self.face_detector = dlib.get_frontal_face_detector()
        self.marker = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')
        self.aligner = _FaceAligner(self.marker,
                                    desiredFaceWidth=size, desiredFaceHeight=size,
                                    desiredLeftEye=(0.23, 0.23))

    def align(self, image):
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        rects = self.face_detector(image)
        target_rect = None
        if len(rects) > 1:
            # pick larger one
            max_area = 0
            max_rect = None
            for rect in rects:
                if rect.area() > max_area:
                    max_area = rect.area()
                    max_rect = rect
            target_rect = max_rect
        elif len(rects) == 0:
            image = cut_black_edge(image)
            target_rect = dlib.rectangle(0, 0, image.shape[1], image.shape[0])
        else:
            target_rect = rects[0]
        target_rect = dlib.rectangle(max(0, target_rect.left()),
                                     max(0, target_rect.top()),
                                     max(0, target_rect.right()),
                                     max(0, target_rect.bottom()))
        aligned_face = self.aligner.align(image, gray, target_rect)
        return aligned_face


if __name__ == '__main__':
    from process.data_helper import load_train_list
    from process.data_helper import DATA_ROOT
    import random
    import os

    list = load_train_list('4@3')
    while True:
        path = list[random.randint(0, len(list) - 1)][0]
        # special case
        # 'train/3_114_3_1_4/profile/0015.jpg'
        # 'train/3_082_3_1_4/profile/0021.jpg'
        # 'test/001451/profile/0001.jpg'

        print(path)
        path = os.path.join(DATA_ROOT, path)
        image = cv2.imread(path)
        aligner = FaceAligner()
        new = aligner.align(image)
        cv2.imshow('aligned', new)
        cv2.waitKey()