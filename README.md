# Source Code for Chalearn Single-modal (RGB) Cross-ethnicity Face anti-spoofing Recognition Challenge @ CVPR2020 by LeoHou
My solution for the [ChaLearn Face Anti-spoofing Attack Detection Challenge](https://competitions.codalab.org/competitions/22151).

## Recent Update

**`2020.2.25`**: code upload for the organizers to reproduce.

#### Prerequisite
* ##### Expected dataset directory structure
    ```
    /path/to/cvpr20/dataset/
        |─  4@1_train.txt
        |─  4@2_train.txt
        |─  4@3_train.txt
        |─  4@1_dev_ref.txt
        |─  4@2_dev_ref.txt
        |─  4@3_dev_ref.txt
        |─  4@1_test_res.txt
        |─  4@2_test_res.txt
        |─  4@3_test_res.txt
        |─  train/
        |─  dev/
        └─  test/
    ```
* ##### Install python packages
    ```
    pip3 install -r requirements.txt
    ```    
<br>

#### Train local feature models
Run training script using the following command for training 3 models for each protocol with 3 different patch size (32, 48, 64). \
It will generate 3 models for each protocol, so total 6 models will be generated. \
The script is designed for 3 GPUs environment. If you don't have enough GPUs, please modify CUDA_VISIBLE_DEVICES variable in the script or train a model at a time.

```shell script
DATA_ROOT=/path/to/cvpr20/dataset/ ./cvpr-train-cnn
```
The results will be generate at <b>models_4@1/, models_4@2/, models_4@3/</b> folders

#### Train dynamic feature model
```shell script
DATA_ROOT=/path/to/cvpr20/dataset/ ./cvpr-train-rnn
```
The results will be generate at <b>rnn_models_4@1/, rnn_models_4@2/, rnn_models_4@3/</b> folders

<br>

#### Infer validation(dev) and test set using trained models
```shell script
# for local feature models
DATA_ROOT=/path/to/cvpr20/dataset/ ./cvpr-test-cnn

# for dynamic feature models
DATA_ROOT=/path/to/cvpr20/dataset/ ./cvpr-test-rnn
```
The results will be generate at the corresponding folders. \
There are total 9 results for local feature models and 3 results for dynamic feature models. \
e.g.
>1. cnn_models_4@1/model_A_color_32/checkpoint/global_test_36_TTA/global_min_acer_model.pth_noTTA.txt
>2. rnn_models_4@1/params_7_2_48/checkpoint/global_test_36_TTA/global_min_acer_model.pth.txt

<br><br>


#### For origanizer team to reproduce the final submissions on 2/24
unzip the models.zip in the project root folder and infer all the trained models 

#### Ensemble all models prediction results

infer and ensemble all predictions
```
DATA_ROOT=/path/to/cvpr20/dataset/
./cvpr-test-cnn
./cvpr-test-rnn
python3 submission.py
```
The ensemble results are generate at 0224_super_4@1.txt, 0224_super_4@1.txt, 0224_super_4@1.txt
Then, concatenate all results to get the final submission in phase2:
```
cat 0224_super_4@*.txt >> 0224_final_submisssion.txt
```








