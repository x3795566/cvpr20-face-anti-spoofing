from metric import *
from process.data_fusion import *


def load_sub(sub):
    sub_dict = {}
    ids = []
    f = open(sub, 'r')

    lines = f.readlines()

    for line in lines:
        line = line.strip()
        line = line.split(' ')
        sub_dict[line[0]] = float(line[1])
        ids.append(line[0])

    return sub_dict, ids


def ensemble_test_dir(sub_dir_list, save_name):
    dict_list = []
    ids = []
    for sub_dir in sub_dir_list:
        for sub in os.listdir(sub_dir):
            if '.txt' in sub:
                sub_dict, ids = load_sub(os.path.join(sub_dir, sub))
                dict_list.append(sub_dict)

    fout = open(save_name, 'w')

    for id in ids:
        prob_tmp = 0.0
        for sub_dict in dict_list:
            prob_tmp += sub_dict[id] / (len(dict_list) * 1.0)
        out = id + ' ' + str(prob_tmp) + '\n'
        fout.write(out)

    fout.close()


def sub_0224(proto):
    dir = r'./models' + r'_' + proto + r'/'
    rnn_dir = r'./rnn_models' + '_' + proto + '/'

    dir_list = [dir + r'model_A_color_32/checkpoint/global_test_36_TTA',
                dir + r'model_A_color_48/checkpoint/global_test_36_TTA',
                dir + r'model_A_color_64/checkpoint/global_test_36_TTA',
                rnn_dir + r'params_7_2_48/checkpoint/global_test_36_TTA']

    ensemble_test_dir(dir_list, '0224_super_' + proto + '.txt')
    print('done!')


def sub_0226(proto):
    dir = r'./models' + r'_' + proto + r'/'
    rnn_dir = r'./rnn_models' + '_' + proto + '/'

    dir_list = [dir + r'model_A_color_48/checkpoint/global_test_36_TTA',
                dir + r'model_A_color_64/checkpoint/global_test_36_TTA',
                rnn_dir + r'params_7_2_48/checkpoint/global_test_36_TTA',
                rnn_dir + r'params_5_2_48/checkpoint/global_test_36_TTA']

    ensemble_test_dir(dir_list, '0226_super_' + proto + '.txt')
    print('done!')


if __name__ == '__main__':
    sub_0226('4@1')
    sub_0226('4@2')
    sub_0226('4@3')
